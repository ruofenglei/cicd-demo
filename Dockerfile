FROM golang:1.13 as builder
WORKDIR /code
COPY go.mod go.sum /code/
RUN go version \
    && go env -w GOPROXY=https://goproxy.io,direct \
    && go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o cicd-demo .

FROM alpine
COPY --from=builder /code /app
WORKDIR /app
ENTRYPOINT ["/app/cicd-demo"]