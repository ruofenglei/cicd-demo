# GitLab CICD Demo

使用 GitLab CI 实现自动化构建部署容器

此 demo 部署应用的访问地址：

[https://al.lrf.pw:8888](https://al.lrf.pw:8888)

**TOC**
- [GitLab CICD Demo](#gitlab-cicd-demo)
  - [Docker 宿主机配置](#docker-%e5%ae%bf%e4%b8%bb%e6%9c%ba%e9%85%8d%e7%bd%ae)
    - [1.生成 CA Key](#1%e7%94%9f%e6%88%90-ca-key)
    - [2.生成 Server Key/Cert](#2%e7%94%9f%e6%88%90-server-keycert)
    - [3.生成 Client Key](#3%e7%94%9f%e6%88%90-client-key)
    - [4.最终有效产物](#4%e6%9c%80%e7%bb%88%e6%9c%89%e6%95%88%e4%ba%a7%e7%89%a9)
    - [5.Client - Server 的访问](#5client---server-%e7%9a%84%e8%ae%bf%e9%97%ae)
    - [5.Docker Server 配置](#5docker-server-%e9%85%8d%e7%bd%ae)
    - [Docker Client 配置](#docker-client-%e9%85%8d%e7%bd%ae)
    - [Reference](#reference)
  - [GitLab CI 配置](#gitlab-ci-%e9%85%8d%e7%bd%ae)
    - [远程访问 Docker 相关变量配置](#%e8%bf%9c%e7%a8%8b%e8%ae%bf%e9%97%ae-docker-%e7%9b%b8%e5%85%b3%e5%8f%98%e9%87%8f%e9%85%8d%e7%bd%ae)
    - [推送镜像相关变量配置](#%e6%8e%a8%e9%80%81%e9%95%9c%e5%83%8f%e7%9b%b8%e5%85%b3%e5%8f%98%e9%87%8f%e9%85%8d%e7%bd%ae)
    - [.gitlab-ci.yml 文件配置](#gitlab-ciyml-%e6%96%87%e4%bb%b6%e9%85%8d%e7%bd%ae)

## Docker 宿主机配置

**目的**：使用 gitlab-runner 访问远程 docekr daemon 以部署容器

### 1.生成 CA Key

-   `ca-key.pem`
-   `ca.pem`

```
# 生成ca私钥,
# -aes256 用于输入一串phrase密码来加密此私钥，后续使用此私钥对client和server的证书签名时需要输入这个密码
# ca私钥很重要因此长度可以多一点，此处为4096
openssl genrsa -aes256 -out ca-key.pem 4096

# 根据此ca私钥生成证x509格式的证书，证书=公钥+签名，用于安全地分发公钥
# 证书必须有过期时间，此处设置为3650天
openssl req -new -x509 -days 3650 -key ca-key.pem -sha256 -out ca.pem
```

### 2.生成 Server Key/Cert

-   `server-key.pem`
-   `server.csr` 签完证书后就可以删了
-   `server-cert-pem`

```shell
# 生成server-key
openssl genrsa -out server-key.pem 4096
# 生成server.csr，csr文件用于向ca请求证书签名，其中$HOST替换为需要访问server的hostname，域名或ip地址
openssl req -subj "/CN=$HOST" -sha256 -new -key server-key.pem -out server.csr

# 使用刚刚生成的csr + ca.pem + ca-key.pem 生成server证书，需要输入ca私钥的密码
openssl x509 -req -days 3650 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out server-cert.pem
```

### 3.生成 Client Key

-   `key.pem`
-   `client.csr` 签完证书后就可以删了
-   `cert.pem`

```shell
# 生成client-key
openssl genrsa -out key.pem 4096
# 生成client.csr，csr文件用于向ca请求证书签名，此处为client的证书所以不需要指定hostname
openssl req -subj '/CN=client' -new -key key.pem -out client.csr

# 使用刚刚生成的csr + ca.pem + ca-key.pem 生成client证书，需要输入ca私钥的密码
openssl x509 -req -days 3650 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out cert.pem
```

### 4.最终有效产物

-   `ca.pem` `ca-key.pem` CA 的公私钥对
-   `server-key.pem` `server-cert.pem` 服务端的 私钥和证书
-   `key.pem` `cert.pem` 客户端的 私钥和证书

### 5.Client - Server 的访问

服务端启动需要：

-   `ca.pem` ca 证书
-   `server-cert.pem` server 证书
-   `server-key.pem` server 私钥

客户端访问需要：

-   `ca.pem` ca 证书
-   `cert.pem` client 证书
-   `key.pem` client 私钥

### 5.Docker Server 配置

_对于以 Systemd 方式启动的 Dockerd：_

修改 `/lib/systemd/system/docker.service` :

```diff
- ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
+ ExecStart=/usr/bin/dockerd --tlsverify --tlscacert=/home/ruofeng/dockerd-secure/ca.pem --tlscert=/home/ruofeng/dockerd-secure/server-cert.pem --tlskey=/home/ruofeng/dockerd-secure/server-key.pem -H=0.0.0.0:2375 --containerd=/run/containerd/containerd.sock
```

然后重启 docker daemon

```shell
sudo systemctl daemon-reload
sudo service docker restart
```

### Docker Client 配置

```shell
# 将 ca.pem cert.pem key.pe 移动到需要client机器的指定目录，默认为 ~/.docker/
export DOCKER_CERT_PATH=~/.docker/certs.d/
export DOCKER_HOST=tcp://$HOST:2375 # 这里的host为上面给server签证书时填入的hostname，ip或域名
export DOCKER_TLS_VERIFY=1
```

就可以在远程客户端（MacBook）上正常的使用 `docker` 命令了，与本地使用 Docker 无异。

### Reference

[Protect the Docker daemon socket](https://docs.docker.com/engine/security/https/)

[Docker Remote API 的安全配置 - p0's blog | 破](https://p0sec.net/index.php/archives/115/)

## GitLab CI 配置

### 远程访问 Docker 相关变量配置

gitlab-runner 作为 docker client 访问远程的目标部署机器，需要

-   ca.pem
-   cert.pem
-   key.pe

此三个文件，在仓库的 settings -> CI/CD -> Variables 中添加变量，由于证书私钥等内容有换行，因此存 base64 encode 后的内容：

-   TARGET_DOCKER_CA # base64 后的 ca.pem
-   TARGET_DOCKER_CERT # base64 后的 cert.pem
-   TARGET_DOCKER_KEY # base64 后的 key.pem
-   TARGET_DOCKER_HOST # 运行有 docker daemon 的部署目标机器，如 tcp://1.2.3.4:2375

### 推送镜像相关变量配置

这里部署目标机器为阿里云上的虚拟机，镜像产物推送到阿里云的镜像仓库，因此还需要账号和密码两个变量

-   ALIYUN_REGISTRY_PASSWORD
-   ALIYUN_REGISTRY_USER

### `.gitlab-ci.yml` 文件配置

此 demo 的 CICD 逻辑为当 master 分支上有 push 或有 tag push 事件时：

1. 根据 Dockerfile 自动构建镜像并推送到阿里云镜像仓库
2. 构建推送完成后在目标部署机器上先拉取刚刚推送的镜像
3. 停止并删除就旧的容器
4. 使用新镜像运行新的容器

具体 yml 文件配置和注释见仓库：

[.gitlab-ci.yml](https://gitlab.com/ruofenglei/cicd-demo/blob/master/.gitlab-ci.yml)
